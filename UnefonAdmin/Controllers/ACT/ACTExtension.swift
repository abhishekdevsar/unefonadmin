//
//  ACTExtension.swift
//  UnefonAdmin
//
//  Created by Shalini Sharma on 17/9/19.
//  Copyright © 2019 Shalini Sharma. All rights reserved.
//

import Foundation

extension ACTVC
{
    func callAPIforClickedHeader()
    {
        dictSelectedFilter["product_id"] = check_WhichHeaderSelected
        
        self.showSpinnerWith(title: "Cargando...")
        let param: [String:Any] = dictSelectedFilter
        //  print(param)
        WebService.requestService(url: ServiceName.POST_getACTreport.rawValue, method: .post, parameters: param, headers: [:], encoding: "URL", viewController: self) { (json:[String:Any], jsonString:String, error:Error?) in
            self.hideSpinner()
           // print(jsonString)
            
            if error != nil
            {
                // Error
                print("Error - ", error!)
                self.showAlertWithTitle(title: "Error", message: "\(error!.localizedDescription)", okButton: "Ok", cancelButton: "", okSelectorName: nil)
                return
            }
            else
            {
                if let internalCode:Int = json["internal_code"] as? Int
                {
                    if internalCode != 0
                    {
                        // Display Error
                        if let msg:String = json["message"] as? String
                        {
                            self.showAlertWithTitle(title: "Error", message: msg, okButton: "Ok", cancelButton: "", okSelectorName: nil)
                            return
                        }
                    }
                    else
                    {
                        // Pass
                        DispatchQueue.main.async {
                            if let di:[String:Any] = json["response_object"] as? [String:Any]
                            {
                                self.dictMain = di
                                self.setUpCollView()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setUpCollView()
    {
        arrData.removeAll()
        collView.reloadData()
        
        if let a1:[[String:Any]] = dictMain["channel_composition"] as? [[String:Any]]
        {
            arrData.append(["title":"Participación Por Canal", "data":a1]) // Pie chart
        }
        if let d2:[String:Any] = dictMain["sales_table"] as? [String:Any]
        {
            if let title:String = d2["table_name"] as? String
            {
                arrData.append(["title":title, "data":d2])  // Grid
            }
        }
        if let a3:[[String:Any]] = dictMain["daily_sales"] as? [[String:Any]]
        {
            arrData.append(["title":"Activaciones Por Día", "data":a3]) // Combined Chart 1
        }
        if let d4:[String:Any] = dictMain["channels_table"] as? [String:Any]
        {
            if let title:String = d4["table_name"] as? String
            {
                arrData.append(["title":title, "data":d4])  // Custom 2 column TableView
            }
        }
        if let a5:[[String:Any]] = dictMain["weekly_sales_counters"] as? [[String:Any]]
        {
            arrData.append(["title":"Activaciones Por Semana", "data":a5]) // Combined Chart 2
        }
        if let a6:[[String:Any]] = dictMain["best_states"] as? [[String:Any]]
        {
            let countArr:Int = a6.count
            arrData.append(["title":"Los \(countArr) Estados con Mayor Desempeño", "data":a6]) // horz Bar Chart 1
        }
        if let a7:[[String:Any]] = dictMain["best_distributors"] as? [[String:Any]]
        {
            let countArr:Int = a7.count
            arrData.append(["title":"Los \(countArr) Distribuidores con Mayor Desempeño", "data":a7]) // horz Bar Chart 2
        }
        if let a8:[[String:Any]] = dictMain["worst_states"] as? [[String:Any]]
        {
            let countArr:Int = a8.count
            arrData.append(["title":"Los \(countArr) Estados con Menor Desempeño", "data":a8]) // horz Bar Chart 1
        }
        if let a9:[[String:Any]] = dictMain["worst_distributors"] as? [[String:Any]]
        {
            let countArr:Int = a9.count
            arrData.append(["title":"Los \(countArr) Distribuidores con Menor Desempeño", "data":a9]) // horz Bar Chart 2
        }
        
        collView.reloadData()
    }
    /*
    -
    - Los 7 Estados con Menor Desempeño
    - Los 7 Distribuidores con Mayor Desempeño
    -
 */
}
