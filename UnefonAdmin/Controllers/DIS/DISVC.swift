
//
//  DISVC.swift
//  UnefonAdmin
//
//  Created by Shalini Sharma on 19/9/19.
//  Copyright © 2019 Shalini Sharma. All rights reserved.
//

import UIKit

class DISVC: UIViewController {

    @IBOutlet weak var lblNoData:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var viewGridContainer:UIView!
    @IBOutlet weak var lblHeader_Day:UILabel!
    @IBOutlet weak var lblHeader_Week:UILabel!
    @IBOutlet weak var vHeader_Day:UIView!
    @IBOutlet weak var vHeader_Week:UIView!
    @IBOutlet weak var c_vHead1_Wd:NSLayoutConstraint!
    
    var dictMain:[String:Any] = [:]
    var dictSelected:[String:Any] = [:]
    
    var arr_First_Week:[[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        k_helper.reffAll_CollV_ForGrid_1.removeAll()
        k_helper.reffAll_CollV_ForGrid_2.removeAll()
        k_helper.reffAll_CollV_ForGrid_3.removeAll()

        if isPad == true
        {
            c_vHead1_Wd.constant = 250
        }
        else
        {
            c_vHead1_Wd.constant = 120
            lblHeader_Day.font = UIFont(name: CustomFont.regular, size: 10)
            lblHeader_Week.font = UIFont(name: CustomFont.regular, size: 10)
        }
        
       // print(dictMain)
        
        if dictMain.count == 0
        {
            lblNoData.isHidden = false
        }
        else
        {
            lblNoData.isHidden = true
        }
        
        if let di:[String:Any] = dictMain["daily_table"] as? [String:Any]
        {
            dictSelected = di
        }
        
        lblTitle.text = "ACTIVACIONES POR DÍA"
        setUpGrid()
    }

    @IBAction func btnHeaderDayClick(btn:UIButton)
    {
        // Day
        lblTitle.text = "ACTIVACIONES POR DÍA"
        lblHeader_Day.textColor = UIColor(named: "Yellow")
        vHeader_Day.backgroundColor = UIColor(named: "Yellow")
        lblHeader_Week.textColor = .black
        vHeader_Week.backgroundColor = .black
        
        if let di:[String:Any] = dictMain["daily_table"] as? [String:Any]
        {
            dictSelected = di
        }
        setUpGrid()
    }
    
    @IBAction func btnHeaderWeekClick(btn:UIButton)
    {
        // Week
        lblTitle.text = "ACTIVACIONES POR SEMANA"
        lblHeader_Day.textColor = .black
        vHeader_Day.backgroundColor = .black
        lblHeader_Week.textColor = UIColor(named: "Yellow")
        vHeader_Week.backgroundColor = UIColor(named: "Yellow")
        
        arr_First_Week.removeAll()
        
        if let di:[String:Any] = dictMain["weekly_table"] as? [String:Any]
        {
            dictSelected = di
            
            if let a:[[String:Any]] = dictSelected["headers"] as? [[String:Any]]
            {
                for d in a
                {
                    if let str:String = d["header_name"] as? String
                    {
                        arr_First_Week.append(["value":str])
                    }
                }
            }
            
        }
        setUpGrid()
    }
    
    func setUpGrid()
    {
        for views in viewGridContainer.subviews
        {
            views.removeFromSuperview()
        }
                
        if let arAll:[[[String:Any]]] = dictSelected["content"] as? [[[String:Any]]]
        {
           
            if arAll.count > 0
            {
                let a_First:[[String:Any]] = arAll.first!
                var arRest:[[[String:Any]]] = []
                for index in 1..<arAll.count
                {
                    let aEach:[[String:Any]] = arAll[index]
                    arRest.append(aEach)
                }
                                
                let controller: GridVC = AppStoryBoards.Customs.instance.instantiateViewController(withIdentifier: "GridVC_ID") as! GridVC
                
                if lblTitle.text == "ACTIVACIONES POR SEMANA"
                {
                    // WEEK
                    controller.arrFirstSection = arr_First_Week
                    controller.arrMain = arAll
                }
                else
                {
                    controller.arrFirstSection = a_First
                    controller.arrMain = arRest
                }
                
                controller.isShowTopBlackHeaderTitlesInCenter = false
                controller.useValue_ForAllCollScrollsHorz = 3
                controller.view.frame = viewGridContainer.bounds;
                controller.willMove(toParent: self)
                viewGridContainer.addSubview(controller.view)
                self.addChild(controller)
                controller.didMove(toParent: self)
            }
        }
        
    }
}
