//
//  BaseVC.swift
//  UnefonAdmin
//
//  Created by Shalini Sharma on 7/9/19.
//  Copyright © 2019 Shalini Sharma. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    @IBOutlet weak var tblViewMenu:UITableView!
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var btnFilter:UIButton!
    @IBOutlet weak var imgVLogo:UIImageView!
    
    @IBOutlet weak var viewContainer:UIView!
    @IBOutlet weak var viewFilter:UIView!
    @IBOutlet weak var viewFilterInner:UIView!
    
    @IBOutlet weak var c_vHeader_Ht:NSLayoutConstraint!
    @IBOutlet weak var c_vMenu_Wd:NSLayoutConstraint!
    @IBOutlet weak var c_lblHead_Wd:NSLayoutConstraint!
    @IBOutlet weak var c_vFilter_Tr:NSLayoutConstraint!
    @IBOutlet weak var c_vFilter_Wd:NSLayoutConstraint!
    @IBOutlet weak var c_imgLogo_Wd:NSLayoutConstraint!
    @IBOutlet weak var c_bLogout_Ht:NSLayoutConstraint!
    @IBOutlet weak var c_lHead_Bt:NSLayoutConstraint!
    @IBOutlet weak var c_bFilter_Wd:NSLayoutConstraint!
    @IBOutlet weak var c_bFilter_Ht:NSLayoutConstraint!
    @IBOutlet weak var c_bFilterVFilter_Horz:NSLayoutConstraint!
    
    var check_FilterOpen = false
    var selectedDashboardName = "VS"    
    
    var arrMenu:[[String:Any]] = [["title":"VS", "selected":true], ["title":"ACT", "selected":false], ["title":"DIS", "selected":false], ["title":"INS", "selected":false], ["title":"COB", "selected":false]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblViewMenu.delegate = self
        tblViewMenu.dataSource = self
        
        lblHeader.text = "Activaciones INAR vs CIS"
        //viewContainer.backgroundColor = UIColor.red
        viewFilterInner.backgroundColor = UIColor(named: "Purple")
        btnFilter.layer.cornerRadius = 5.0
        
        if isPad == false
        {
            c_bFilter_Wd.constant = 30
            c_bFilter_Ht.constant = 30
            c_bFilterVFilter_Horz.constant = -5
            btnFilter.setImage(UIImage(named: "filterPhn"), for: .normal)
        }
        
        viewFilter.alpha = 0
        self.perform(#selector(self.showFilterButton), with: nil, afterDelay: 0.1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushTokenToServer(_:)), name: Notification.Name("pushTokenToServer"), object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterValuesModified(_:)), name: NSNotification.Name(rawValue: "filterModified"), object: nil)

        if k_helper.checkCallPushTokenApi == true
        {
            callApiPushToken()
        }
        
        setBaseViewContainer(str: selectedDashboardName, resetFilter: true, dict2Pas: [:])
    }
    
    @objc func showFilterButton()
    {
        if isPad
        {
            lblHeader.font = UIFont(name: CustomFont.semiBold, size: 19.0)
            c_vHeader_Ht.constant = 80
            c_lblHead_Wd.constant = 300
            c_vMenu_Wd.constant = 90
            c_imgLogo_Wd.constant = 90
            let widthContainer_iPad = UIScreen.main.bounds.width - 90 // 90 is Menu table View
            
            var widthFilter_iPad:CGFloat = 0.0
            
            if UIDevice.current.orientation == .portrait || UIDevice.current.orientation == .portraitUpsideDown
            {
                widthFilter_iPad = widthContainer_iPad/2.5
            }
            else
            {
                widthFilter_iPad = widthContainer_iPad/3.7
            }
            
            c_vFilter_Wd.constant = widthFilter_iPad
            c_vFilter_Tr.constant = 50 - widthFilter_iPad // 50 because of button filter, we show extra left over container
        }
        else
        {
            // PHONE
            c_vHeader_Ht.constant = 80
            c_imgLogo_Wd.constant = 42
            imgVLogo.contentMode = .scaleAspectFit
            imgVLogo.clipsToBounds = true
            c_bLogout_Ht.constant = 50
            c_lHead_Bt.constant = 10
            
            lblHeader.font = UIFont(name: CustomFont.semiBold, size: 13.0)
            let widthContainer_iPhone = UIScreen.main.bounds.width - 70 // 70 is Menu table View
            let widthFilter_iPhone = widthContainer_iPhone/1.2
            c_vFilter_Wd.constant = widthFilter_iPhone
            c_vFilter_Tr.constant = 50 - widthFilter_iPhone // 50 because of button filter, we show extra left over container
        }
        
        UIView.animate(withDuration: 1.0) {
            self.viewFilter.alpha = 1.0
        }
    }

    // handle notification
    @objc func filterValuesModified(_ notification: NSNotification)
    {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary?
        {
            check_FilterOpen = true
            btnShowFilterClick(btn: UIButton())

            print("- Call Api to load dashboard - ", selectedDashboardName)
            // Call API to Show Dashboard, ViewContainer
            callAPIToLoadDashBoardFor(strDashboardName: selectedDashboardName, payload:dict as! [String : Any])
        }
    }
    
    @objc func pushTokenToServer(_ notification: NSNotification)
       {
            print("- Call Api to upload push token - ", deviceToken_FCM)
            callApiPushToken()
       }
    
    @IBAction func btnShowFilterClick(btn:UIButton)
    {
        check_FilterOpen = !check_FilterOpen
        
        if isPad
        {
            if self.check_FilterOpen == true
            {
                // show view
                self.c_vFilter_Tr.constant = 0
            }
            else
            {
                // hide view
                if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight
                {
                    let widthContainer_iPad = UIScreen.main.bounds.height - 90 // 90 is Menu table View
                    let widthFilter_iPad = widthContainer_iPad/2.5
                    self.c_vFilter_Tr.constant = 50 - widthFilter_iPad
                }
                else
                {
                    // portrait
                    let widthContainer_iPad = UIScreen.main.bounds.width - 90 // 90 is Menu table View
                    let widthFilter_iPad = widthContainer_iPad/2.5
                    self.c_vFilter_Tr.constant = 50 - widthFilter_iPad
                }
            }
        }
        else
        {
            if self.check_FilterOpen == true
            {
                // show view
                self.c_vFilter_Tr.constant = 0
            }
            else
            {
                // hide view
                let widthContainer_iPhone = UIScreen.main.bounds.width - 70 // 90 is Menu table View
                let widthFilter_iPhone = widthContainer_iPhone/1.1
                c_vFilter_Tr.constant = 50 - widthFilter_iPhone // 50 because of button, we show more over container

            }
        }
    }
    
}

extension BaseVC
{
    // MARK: - Lock Orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        return (isPad == true) ? .all : .portrait
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        print("--> iPAD Screen Orientation")
        check_FilterOpen = false
        if UIDevice.current.orientation.isLandscape
        {
            print("landscape, Hide Filter View")
            let widthContainer_iPad = UIScreen.main.bounds.height - 90 // 90 is Menu table View
            let widthFilter_iPad = widthContainer_iPad/2.5
            c_vFilter_Tr.constant = 50 - widthFilter_iPad
        }
        else
        {
            print("portrait, Hide Filter View")
            let widthContainer_iPad = UIScreen.main.bounds.width - 90 // 90 is Menu table View
            let widthFilter_iPad = widthContainer_iPad/2.5
            c_vFilter_Tr.constant = 50 - widthFilter_iPad
        }
    }
}

extension BaseVC: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        // on basis of viewMenu Width
        return (isPad == true) ? 90 : 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // MENU
        let cell:CellMenu_Rows = tblViewMenu.dequeueReusableCell(withIdentifier: "CellMenu_Rows", for: indexPath) as! CellMenu_Rows
        cell.selectionStyle = .none
        cell.lblTitle.text = ""
        cell.lblTitle.textColor = UIColor.white
        
        if isPad
        {
            cell.c_lbl_Wd.constant = 64
            cell.c_lbl_Ht.constant = 64
            
            cell.lblTitle.layer.cornerRadius = 32.0
            cell.lblTitle.font = UIFont(name: CustomFont.semiBold, size: 22.0)
        }
        else
        {
            cell.c_lbl_Wd.constant = 44
            cell.c_lbl_Ht.constant = 44
            
            cell.lblTitle.layer.cornerRadius = 22.0
            cell.lblTitle.font = UIFont(name: CustomFont.semiBold, size: 16.0)
        }

        cell.lblTitle.layer.borderColor = UIColor.white.cgColor
        cell.lblTitle.layer.borderWidth = 3.0
        cell.lblTitle.layer.masksToBounds = true

        let di:[String:Any] = arrMenu[indexPath.row]
        
        if let strMenu:String = di["title"] as? String
        {
            cell.lblTitle.text = strMenu
        }
        
        if let sel:Bool = di["selected"] as? Bool
        {
            if sel == true
            {
                cell.lblTitle.layer.borderColor = UIColor(named: "Yellow")?.cgColor
                cell.lblTitle.textColor = UIColor(named: "Yellow")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let di:[String:Any] = arrMenu[indexPath.row]
        if let strMenu:String = di["title"] as? String
        {
            selectedDashboardName = strMenu
            k_helper.firstTimeOnly_CallFilterClickProgramatically = true
            setBaseViewContainer(str: strMenu, resetFilter: true, dict2Pas: [:])
        }
        
        for index in 0..<arrMenu.count
        {
            var dict:[String:Any] = arrMenu[index]
            dict["selected"] = false
            
            if indexPath.row == index
            {
                dict["selected"] = true
            }
            
            arrMenu[index] = dict
        }
        
        tableView.reloadData()
    }
    
    func setBaseViewContainer(str:String, resetFilter:Bool, dict2Pas:[String:Any])
    {
        print("you Tapped on - ", str)

        viewFilter.isHidden = false
        
        if resetFilter == true
        {
            for views in viewFilterInner.subviews
            {
                views.removeFromSuperview()
            }
            
            let controller: FilterVC = AppStoryBoards.Customs.instance.instantiateViewController(withIdentifier: "FilterVC_ID") as! FilterVC
            controller.view.frame = self.viewFilterInner.bounds;
            controller.willMove(toParent: self)
            self.viewFilterInner.addSubview(controller.view)
            self.addChild(controller)
            controller.didMove(toParent: self)
        }
        
        for views in viewContainer.subviews
        {
            views.removeFromSuperview()
        }
        
        if str == "VS"
        {
            lblHeader.text = "Activaciones INAR vs CIS"
            
            let vc: VSVC = AppStoryBoards.VS.instance.instantiateViewController(withIdentifier: "VSVC_ID") as! VSVC
            vc.dictMain = dict2Pas
            vc.view.frame = self.viewContainer.bounds;
            vc.willMove(toParent: self)
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
        else if str == "ACT"
        {
            lblHeader.text = "Activaciones"
            
            let vc: ACTVC = AppStoryBoards.ACT.instance.instantiateViewController(withIdentifier: "ACTVC_ID") as! ACTVC
            vc.dictFull = dict2Pas
            vc.view.frame = self.viewContainer.bounds;
            vc.willMove(toParent: self)
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
        else if str == "DIS"
        {
            lblHeader.text = "Activaciones Por Distribuidor"
            
            let vc: DISVC = AppStoryBoards.DIS.instance.instantiateViewController(withIdentifier: "DISVC_ID") as! DISVC
            vc.dictMain = dict2Pas
            vc.view.frame = self.viewContainer.bounds;
            vc.willMove(toParent: self)
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
        else if str == "INS"
        {
            viewFilter.isHidden = true
            lblHeader.text = "Plan de Incentivos"
            
            let vc: UINavigationController = AppStoryBoards.INS.instance.instantiateViewController(withIdentifier: "Nav_INS_ID") as! UINavigationController
            vc.view.frame = self.viewContainer.bounds;
            vc.willMove(toParent: self)
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
        else if str == "COB"
        {
            viewFilter.isHidden = true
            lblHeader.text = "Cobertura"
            
            let vc: COBVC = AppStoryBoards.COB.instance.instantiateViewController(withIdentifier: "COBVC_ID") as! COBVC
            vc.view.frame = self.viewContainer.bounds;
            vc.willMove(toParent: self)
            self.viewContainer.addSubview(vc.view)
            self.addChild(vc)
            vc.didMove(toParent: self)
        }
    }
    
    
    @IBAction func logout(btn:UIButton)
    {
        let alertController = UIAlertController(title: "Cerrar sesión", message: "Estás seguro de que quieres desconectarte?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "sí", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("Yes Logout Pressed")
            
            self.callApiDeletePushToken()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Logout Pressed")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
