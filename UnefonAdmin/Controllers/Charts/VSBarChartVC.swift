//
//  VSBarChartVC.swift
//  UnefonAdmin
//
//  Created by Shalini Sharma on 11/9/19.
//  Copyright © 2019 Shalini Sharma. All rights reserved.
//

import UIKit
import Charts

class VSBarChartVC: UIViewController {
    
    @IBOutlet weak var collView:UICollectionView!
    @IBOutlet weak var btnOverlayRight_iPhone:UIButton!
    
    var arr_NumberOfArrayInCollection:[[[String:Any]]] = []
    var maxIndexForFullArray:Int = 0
    var arr_EachIndexCollV:[[String:Any]] = []
    var currentShowingIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        maxIndexForFullArray = arr_NumberOfArrayInCollection.count - 1
        collView.delegate = self
        collView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnLeftClick(btn: UIButton())
    }
    
    @IBAction func btnLeftClick(btn:UIButton)
    {
        print("Left Click")
        currentShowingIndex = currentShowingIndex - 1
        
        if currentShowingIndex <= 0
        {
            currentShowingIndex = 0
            arr_EachIndexCollV = arr_NumberOfArrayInCollection.first!
        }
        else
        {
            arr_EachIndexCollV = arr_NumberOfArrayInCollection[currentShowingIndex]
        }
        collView.reloadData()
    }
    
    @IBAction func btnRightClick(btn:UIButton)
    {
        print("Right Click")
        currentShowingIndex = currentShowingIndex + 1
        
        if  currentShowingIndex >= maxIndexForFullArray
        {
            currentShowingIndex = arr_NumberOfArrayInCollection.count - 1
            arr_EachIndexCollV = arr_NumberOfArrayInCollection[currentShowingIndex]
        }
        else
        {
            arr_EachIndexCollV = arr_NumberOfArrayInCollection[currentShowingIndex]
        }
        collView.reloadData()
    }
}

extension VSBarChartVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_EachIndexCollV.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dict:[String:Any] = arr_EachIndexCollV[indexPath.item]
        
        let cell:CollCellVSBar = collView.dequeueReusableCell(withReuseIdentifier: "CollCellVSBar", for: indexPath) as! CollCellVSBar
        cell.lblWeekName.text = ""
        cell.lblWeekRange.text = ""
        
        if let str:String = dict["week_name"] as? String
        {
            cell.lblWeekName.text = str
        }
        if let str:String = dict["description"] as? String
        {
            cell.lblWeekRange.text = str
        }
        
        if isPad == false
        {
            cell.lblWeekName.font = UIFont(name: CustomFont.regular, size: 10)
            cell.lblWeekRange.font = UIFont(name: CustomFont.regular, size: 7)
        }
        
        cell.showBarChart(dictEachCell: dict)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    //MARK: Use for interspacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    // To make Cells Display in Center
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if isPad
        {
            if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight
            {
                let totalCellWidth = 120 * collectionView.numberOfItems(inSection: 0)
                let totalSpacingWidth = 0
                
                let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
                let rightInset = leftInset
                
                print("- - - - -LANDScape Bar - - - - - - -")
                return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
            }
            else
            {
                let totalCellWidth = 120 * collectionView.numberOfItems(inSection: 0)
                let totalSpacingWidth = 0
                
                let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
                let rightInset = leftInset
                
                print("- - - - - PORTRait Bar- - - - - - -")
                return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
            }
        }
        else
        {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
    }
    
    //MARK: set Cell CGSize
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if isPad
        {
            return CGSize(width: 120, height: 200)
        }
        else
        {
            return CGSize(width: 80, height: 200)
        }
    }
}
